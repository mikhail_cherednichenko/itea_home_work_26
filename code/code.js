// Додати нові продукти (Відео для відео хостингу та страви для ресторану)
// Відео має бути 3 зовнішнім посиланням та 3 відео завантажені до проекту 

// Тут реалізація вашого коду. 

import { getLogin, getPassword } from "./var.js";
import { StoreElementCRM } from "./class.js";
import { generationId, dateNow } from "./functions.js";

console.log(getLogin);
console.log(getPassword);

class videoElementCRM {
    constructor(
        productName = "",
        like = 0,
        dateNow = () => { },
        url = "/img/error.png",
        poster = "/img/error.png",
        id = () => { },
        description = "",
        keywords = [],
        ) {
            this.productName = productName;
            this.like = like;
            this.date = dateNow();
            this.url = url;
            this.poster = poster;
            this.id = id();
            this.description = description;
            this.keywords = keywords.split(",");
            this.status = false;
        }
}

class restElementCRM {
    constructor(
        id = () => { },
        productName = "",
        productWeiht = "",
        ingredients = "",
        Price = 0,
        productimageUrl = "/img/error.png",
        keywords = [],
        Weiht = "",
        stopList = false,
        ageRestrictions = false,
        like = 0,
        dateNow = () => { },
        description = "",
        ) {
            this.id = id();
            this.productName = productName;
            this.productWeiht = productWeiht;
            this.ingredients = ingredients;
            this.Price = Price;
            this.productimageUrl = productimageUrl;
            this.keywords = keywords.split(",");
            this.Weiht = Weiht;
            this.stopList = stopList;
            this.ageRestrictions = ageRestrictions;
            this.like = like;
            this.date = dateNow();
            this.description = description;
            this.status = false;
        }
}

function saveDataStudent() {
    try {
        const [isCategory] = document.querySelector("select").selectedOptions;
        const [...inputs] = document.querySelectorAll("form input");
        if (isCategory.value === "Магазин") {
            const obj = {
                productName: "string",
                porductPrice: "number",
                productImage: "string",
                productDescription: "string",
                keywords: "string array",
            };

            inputs.forEach(e => {
                obj[e.dataset.type] = e.value;
                e.value = ''
            })

            const store = JSON.parse(localStorage.store);
            store.push(new StoreElementCRM(
                obj.productName,
                obj.porductPrice,
                obj.productImage,
                obj.productDescription,
                undefined,
                obj.keywords,
                dateNow,
                generationId));

            localStorage.store = JSON.stringify(store);
        } else if (isCategory.value === "Відео хостинг") {
            const obj = {
                productName: "string",
                poster: "string",
                url: "string",
                description: "string",
                keywords: "string array",
            };

            inputs.forEach(e => {
                obj[e.dataset.type] = e.value;
                e.value = ''
            })

            const video = JSON.parse(localStorage.video);
            video.push(new videoElementCRM(
                obj.productName,
                undefined,
                dateNow,
                obj.url,
                obj.poster,
                generationId,
                obj.description,
                obj.keywords));

            localStorage.video = JSON.stringify(video);
        } else if (isCategory.value === "Рестаран") {
            const obj = {
                productName: "string",
                productWeiht: "string",
                ingredients: "string",
                description: "string",
                keywords: "string array",
                price: "string",
                productimageUrl: "string",
            };

            inputs.forEach(e => {
                obj[e.dataset.type] = e.value;
                e.value = ''
            })

            const rest = JSON.parse(localStorage.rest);
            rest.push(new restElementCRM(
                generationId,
                obj.productName,
                obj.productWeiht,
                obj.ingredients,
                obj.price,
                obj.productimageUrl,
                obj.keywords,
                undefined,
                undefined,
                undefined,
                undefined,
                dateNow,
                obj.description));

            localStorage.rest = JSON.stringify(rest);
        }
    } catch (e) {
        console.error(e)
    }
}

export {saveDataStudent}